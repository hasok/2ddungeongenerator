﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonGen
{
    public class Tile
    {
        public int X { get; }

        public int Y { get; }

        public bool IsTile { get; set; }

        public bool IsBoss { get; set; }

        public bool IsStart { get; set; }

        public Tile(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
