﻿using System;
using System.Collections.Generic;

namespace DungeonGen
{
    public class Program
    {
        public static Tile[,] matrix = new Tile[30, 30];


        //Parameter
        public static int minLengthToBoss = 5;
        public static int maxLengthToBoss = 8;

        //leave 0 to let the algorithm choose this
        public static int maxSideRoomCount = 30;

        public static int sidePathChance = 100;


        private static Random rnd = new Random();
        private static int roomCount;
        private static int currentRoomCount = 0;

        //100 == 100%

        public static void PopulateMatrix()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = new Tile(i, j);
                }
            }
        }

        public static void GenerateTiles()
        {
            PopulateMatrix();
            currentRoomCount = 0;

            Tile startTile = matrix[matrix.GetLength(0) / 2, matrix.GetLength(1) / 2];
            startTile.IsStart = true;
            startTile.IsTile = true;

            int lengthToBoss = rnd.Next(minLengthToBoss, maxLengthToBoss);

            if (maxSideRoomCount == 0)
            {
                roomCount = rnd.Next(lengthToBoss + 6, lengthToBoss + 8);
            }
            else
            {
                roomCount = maxSideRoomCount;
            }

            var bossTile = SetBossTile(startTile, lengthToBoss);


            var directionList = GenerateDirectionToBoss(startTile, bossTile);

            Point currentPoint = new Point(startTile.X, startTile.Y);

            foreach (var direction in directionList)
            {
                Tile tile = null;
                switch (direction)
                {
                    case Directions.Top:
                        tile = matrix[currentPoint.X, currentPoint.Y + 1];
                        currentPoint.Y++;
                        break;
                    case Directions.Bottom:
                        tile = matrix[currentPoint.X, currentPoint.Y - 1];
                        currentPoint.Y--;
                        break;
                    case Directions.Right:
                        tile = matrix[currentPoint.X + 1, currentPoint.Y];
                        currentPoint.X++;
                        break;
                    case Directions.Left:
                        tile = matrix[currentPoint.X - 1, currentPoint.Y];
                        currentPoint.X--;
                        break;
                }
                tile.IsTile = true;
            }

            int startPathCount = rnd.Next(1, 3);
            var excludedDirection = directionList[0];

            int startsidetilescount = 5;
            for (int i = 0; i < startPathCount; i++)
            {

                var direction = (Directions)rnd.Next(1, 4);
                for (int k = 0; k < 4; k++)
                {
                    if (direction == excludedDirection)
                    {
                        direction = (Directions)rnd.Next(1, 4);
                    }
                }

                var size = GenerateSidePath(startTile, direction, startsidetilescount);
                startsidetilescount = startsidetilescount - size;
            }

            //Display();

            GenerateSidePathsAlongMainPath(startTile, directionList);

            Display();
        }

        public static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    GenerateTiles();
                    break;
                }
                catch (Exception ex)
                {

                }

                Console.WriteLine();
                Console.ReadKey();
                Console.WriteLine();
            }
        }

        public static void GenerateSidePathsAlongMainPath(Tile startTile, List<Directions> directionList)
        {
            Point currentPoint = new Point(startTile.X, startTile.Y);

            for (int i = 0; i < directionList.Count - 1; i++)
            {
                Tile tile = null;
                switch (directionList[i])
                {
                    case Directions.Top:
                        tile = matrix[currentPoint.X, currentPoint.Y + 1];
                        currentPoint.Y++;
                        break;
                    case Directions.Bottom:
                        tile = matrix[currentPoint.X, currentPoint.Y - 1];
                        currentPoint.Y--;
                        break;
                    case Directions.Right:
                        tile = matrix[currentPoint.X + 1, currentPoint.Y];
                        currentPoint.X++;
                        break;
                    case Directions.Left:
                        tile = matrix[currentPoint.X - 1, currentPoint.Y];
                        currentPoint.X--;
                        break;
                }
                RollForGenSidepath(tile, 6);
            }
        }

        //return length of the generated sidepath
        public static int RollForGenSidepath(Tile currentTile, int maxsize, Directions? excludeddirection = null)
        {
            if (maxsize <= 0)
            {
                return 0;
            }

            var chance = rnd.Next(1, 100);
            List<Directions> potentialPaths = new List<Directions>();


            if (chance <= sidePathChance)
            {
                if (!matrix[currentTile.X, currentTile.Y + 1].IsTile)
                {
                    if (excludeddirection != Directions.Top)
                    {
                        potentialPaths.Add(Directions.Top);
                    }
                }

                if (!matrix[currentTile.X, currentTile.Y - 1].IsTile)
                {
                    if (excludeddirection != Directions.Bottom)
                    {
                        potentialPaths.Add(Directions.Bottom);
                    }

                }

                if (!matrix[currentTile.X + 1, currentTile.Y].IsTile)
                {
                    if (excludeddirection != Directions.Right)
                    {
                        potentialPaths.Add(Directions.Right);
                    }
                }

                if (!matrix[currentTile.X - 1, currentTile.Y].IsTile)
                {
                    if (excludeddirection != Directions.Left)
                    {
                        potentialPaths.Add(Directions.Left);
                    }
                }
            }
            else
            {
                return 0;
            }

            var pathlength = 0;

            if (potentialPaths.Count > 0)
            {
                Shuffle(potentialPaths);

                for (int i = 0; i < potentialPaths.Count; i++)
                {
                    var size = GenerateSidePath(currentTile, potentialPaths[i], maxsize);
                    pathlength = pathlength + size;
                    maxsize = maxsize - size;
                    if (size > 0)
                    {
                        //66% chance to only spawn path on one side
                        if (rnd.Next(1, 3) >= 2)
                        {
                            break;
                        }
                    }
                }

            }

            return pathlength;
        }

        //returns size of the path
        public static int GenerateSidePath(Tile currentTile, Directions direction, int maxSize)
        {
            var pathlength = 0;

            var length = rnd.Next(1, 4);

            if (currentRoomCount > roomCount)
            {
                return 0;
            }

            if (length > maxSize)
            {
                length = maxSize;
            }

            pathlength = length;

            var pathTileList = new List<Tile>();

            for (int i = 0; i < length; i++)
            {
                Tile nexttile = null;
                switch (direction)
                {
                    case Directions.Top:
                        nexttile = matrix[currentTile.X, currentTile.Y + 1];
                        if (CheckForAdjacent(nexttile, Directions.Bottom))
                        {
                            return 0;
                        }
                        break;
                    case Directions.Bottom:
                        nexttile = matrix[currentTile.X, currentTile.Y - 1];
                        if (CheckForAdjacent(nexttile, Directions.Top))
                        {
                            return 0;
                        }
                        break;
                    case Directions.Right:
                        nexttile = matrix[currentTile.X + 1, currentTile.Y];
                        if (CheckForAdjacent(nexttile, Directions.Left))
                        {
                            return 0;
                        }
                        break;
                    case Directions.Left:
                        nexttile = matrix[currentTile.X - 1, currentTile.Y];
                        if (CheckForAdjacent(nexttile, Directions.Right))
                        {
                            return 0;
                        }
                        break;
                }

                pathTileList.Add(nexttile);
                nexttile.IsTile = true;
                currentRoomCount++;


                currentTile = nexttile;
            }

            var sidepathlength = 0;

            foreach (var tile in pathTileList)
            {
                var sidelength = RollForGenSidepath(tile, maxSize - length - sidepathlength, direction);
                sidepathlength += sidelength;
            }

            return pathlength;
        }

        //returns true if it has adjacent tiles
        public static bool CheckForAdjacent(Tile tileToCheck, Directions comingFrom)
        {
            if (comingFrom != Directions.Top)
            {
                if (matrix[tileToCheck.X, tileToCheck.Y + 1].IsTile)
                {
                    return true;
                }
            }

            if (comingFrom != Directions.Bottom)
            {
                if (matrix[tileToCheck.X, tileToCheck.Y - 1].IsTile)
                {
                    return true;
                }
            }

            if (comingFrom != Directions.Right)
            {
                if (matrix[tileToCheck.X + 1, tileToCheck.Y].IsTile)
                {
                    return true;
                }
            }


            if (comingFrom != Directions.Left)
            {
                if (matrix[tileToCheck.X - 1, tileToCheck.Y].IsTile)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool NextBoolean(Random random)
        {
            return random.Next() > (Int32.MaxValue / 2);
        }

        public static Tile SetBossTile(Tile startTile, int lengthToBoss)
        {
            Random rnd = new Random();

            var direction = rnd.Next(0, 4);

            int xDirection = 0;
            int yDirection = 0;


            switch (direction)
            {
                //top right
                case 0:
                    for (int i = 0; i < lengthToBoss; i++)
                    {
                        var randombool = NextBoolean(rnd);

                        if (randombool)
                        {
                            xDirection++;
                        }
                        else
                        {
                            yDirection++;
                        }
                    }
                    break;
                //bottom right
                case 1:
                    for (int i = 0; i < lengthToBoss; i++)
                    {
                        var randombool = NextBoolean(rnd);

                        if (randombool)
                        {
                            xDirection++;
                        }
                        else
                        {
                            yDirection--;
                        }
                    }
                    break;
                //bottom left
                case 2:
                    for (int i = 0; i < lengthToBoss; i++)
                    {
                        var randombool = NextBoolean(rnd);

                        if (randombool)
                        {
                            xDirection--;
                        }
                        else
                        {
                            yDirection--;
                        }
                    }
                    break;
                //top left
                case 3:
                    for (int i = 0; i < lengthToBoss; i++)
                    {
                        var randombool = NextBoolean(rnd);

                        if (randombool)
                        {
                            xDirection--;
                        }
                        else
                        {
                            yDirection++;
                        }
                    }
                    break;
                default:
                    throw new Exception("");
            }

            var tile = matrix[startTile.X + xDirection, startTile.Y + yDirection];
            tile.IsBoss = true;
            tile.IsTile = true;

            return tile;
        }

        public static List<Directions> GenerateDirectionToBoss(Tile startTile, Tile bossTile)
        {
            List<Directions> pathList = new List<Directions>();

            int xDiff = startTile.X - bossTile.X;
            int yDiff = startTile.Y - bossTile.Y;

            Directions direction1 = (xDiff > 0) ? Directions.Left : Directions.Right;
            Directions direction2 = (yDiff > 0) ? Directions.Bottom : Directions.Top;

            for (int i = 1; i <= Math.Abs(xDiff); i++)
            {
                pathList.Add(direction1);
            }

            for (int l = 1; l <= Math.Abs(yDiff); l++)
            {
                pathList.Add(direction2);
            }

            Shuffle(pathList);

            return pathList;
        }

        public enum Directions
        {
            Top = 0,
            Bottom = 1,
            Right = 2,
            Left = 3
        }

        public static void Shuffle<T>(List<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Display()
        {
            for (int i = matrix.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[j, i].IsTile)
                    {
                        if (matrix[j, i].IsStart)
                        {
                            Console.ForegroundColor = ConsoleColor.Magenta;

                        }
                        else if (matrix[j, i].IsBoss)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                        }
                        Console.Write("X ");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;

                        Console.Write("O ");
                    }
                    //Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
